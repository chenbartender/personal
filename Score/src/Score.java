import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

public class Score {
	public static void main(String[] args) throws IOException {
		File htmlsmall = new File("small.htm");//导入HTML文件
		File htmlall = new File("all.htm");
		Document small = Jsoup.parse(htmlsmall, "UTF-8");//用jsoup包解析HTML文件
		Document all = Jsoup.parse(htmlall, "UTF-8");
		Elements smallelem = small.getElementsByClass("interaction-row");//用类选择器将有关题目的子代选择出来，放入Elements中
		Elements allelem = all.getElementsByClass("interaction-row");
		Double ProgramMy,AddMy,BaseMy,TestMy,BeforeMy;
		ProgramMy=AddMy=BaseMy=TestMy=BeforeMy=0.0;
		Score text = new Score();//调用提取分数的方法
		AddMy=text.Smallfenshu(smallelem, "附加题");
		ProgramMy=text.Smallfenshu(smallelem, "编程题");
		BaseMy=text.Smallfenshu(smallelem, "课堂完成");
		TestMy=text.Smallfenshu(smallelem, "课堂小测");
		BeforeMy=text.Allfenshu(smallelem, "课前自测")+text.Allfenshu(allelem, "课前自测");
		if (ProgramMy>=95) {
			ProgramMy=95.0;
		}
		if (AddMy>=90) {
			AddMy=90.0;
		}
		double zhongfeng = BaseMy*0.3*0.95+TestMy*0.2+BeforeMy*0.25+ProgramMy*0.1+AddMy*0.05;
		Properties pz=new Properties();
		pz.load(new FileInputStream("total.properties"));//导入分数配置文件
		double BeforeSure = Double.parseDouble(pz.getProperty("before"));//提取赢得课前自测分数
		double BaseSure = Double.parseDouble(pz.getProperty("base"));//提取课堂完成应得分数
		double TestSure = Double.parseDouble(pz.getProperty("test"));//提取课堂小测应得分数
		double ProgramSure = Double.parseDouble(pz.getProperty("program"));//提取编程题应得分数
		double AddSure = Double.parseDouble(pz.getProperty("add"));//提取附加题应得分数
		double Sure= BaseSure*0.3*0.95+TestSure*0.2+BeforeSure*0.25+ProgramSure*0.1+AddSure*0.05;
		System.out.println(zhongfeng/Sure*100);	
	}
	//提取除课前小测外的分数的方法
	public Double Smallfenshu(Elements smallelem,String leixing) {
		Double FS=0.0;
		//遍历题目数组
		for (int i = 0; i < smallelem.size(); i++) {
			Element smallzhang = smallelem.get(i);//获取当前题目的节点
			String mz = smallzhang.child(1).child(0).text();//获取当前题目的名称
			String fs = smallzhang.child(1).child(2).child(0).text();//获取当前题目的经验
			if (mz.indexOf(leixing)>=0) {//判断题目的种类
				if (fs.indexOf("已参与")>=0) {//判断该题是否参与
					if (fs.indexOf("互评")>=0) {//判断该题是否有互评经验
						String canyu = smallzhang.child(1).child(2).child(0).child(7).text().replaceAll("经验", "").replaceAll("互评","").replaceAll(" ", "");
						int cy = Integer.parseInt(canyu);
						String huping = smallzhang.child(1).child(2).child(0).child(9).text().replaceAll("经验", "").replaceAll("互评","").replaceAll(" ", "");
						int hp = Integer.parseInt(huping);
						FS+=cy+hp;
					}
					else {
						String canyu = smallzhang.child(1).child(2).child(0).child(7).text().replaceAll("经验", "").replaceAll("互评","").replaceAll(" ", "");
						int cy = Integer.parseInt(canyu);
						FS+=cy;
					}
				}
			}
		}
		return FS;
	}
	//提取课前小测的方法
	public Double Allfenshu(Elements allelem,String leixing) {
		Scanner reader=null;
		Double FS=0.0;
		for (int i = 0; i < allelem.size(); i++) {
			Element allzhang = allelem.get(i);
			if(allzhang.toString().contains("课前自测")) {
				Elements span = allzhang.getElementsByTag("span");
				for(int j = 0; j < span.size(); j++) {
					if(span.get(j).text().contains("经验") && span.get(j).toString().contains("color:#8FC31F")) {
						reader = new Scanner(span.get(j).text());
						FS += reader.nextDouble();
						break;
					}
				}
			}
		}
		return FS;
	}
}
